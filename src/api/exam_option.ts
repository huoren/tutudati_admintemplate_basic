import request from '@/utils/request'

// 单选试题列表
export function apiExamOptionLists(params: any) {
    return request.get({ url: '/exam.exam_option/lists', params })
}

// 添加单选试题
export function apiExamOptionAdd(params: any) {
    return request.post({ url: '/exam.exam_option/add', params })
}

// 编辑单选试题
export function apiExamOptionEdit(params: any) {
    return request.post({ url: '/exam.exam_option/edit', params })
}

// 删除单选试题
export function apiExamOptionDelete(params: any) {
    return request.post({ url: '/exam.exam_option/delete', params })
}

// 单选试题详情
export function apiExamOptionDetail(params: any) {
    return request.get({ url: '/exam.exam_option/detail', params })
}