import request from '@/utils/request'

// 问答试题列表
export function apiExamReadingLists(params: any) {
    return request.get({ url: '/exam.exam_reading/lists', params })
}

// 添加问答试题
export function apiExamReadingAdd(params: any) {
    return request.post({ url: '/exam.exam_reading/add', params })
}

// 编辑问答试题
export function apiExamReadingEdit(params: any) {
    return request.post({ url: '/exam.exam_reading/edit', params })
}

// 删除问答试题
export function apiExamReadingDelete(params: any) {
    return request.post({ url: '/exam.exam_reading/delete', params })
}

// 问答试题详情
export function apiExamReadingDetail(params: any) {
    return request.get({ url: '/exam.exam_reading/detail', params })
}