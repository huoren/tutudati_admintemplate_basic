import request from '@/utils/request'

// 判断试题列表
export function apiExamJudeLists(params: any) {
    return request.get({ url: '/exam.exam_jude/lists', params })
}

// 添加判断试题
export function apiExamJudeAdd(params: any) {
    return request.post({ url: '/exam.exam_jude/add', params })
}

// 编辑判断试题
export function apiExamJudeEdit(params: any) {
    return request.post({ url: '/exam.exam_jude/edit', params })
}

// 删除判断试题
export function apiExamJudeDelete(params: any) {
    return request.post({ url: '/exam.exam_jude/delete', params })
}

// 判断试题详情
export function apiExamJudeDetail(params: any) {
    return request.get({ url: '/exam.exam_jude/detail', params })
}