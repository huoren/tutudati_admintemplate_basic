import request from '@/utils/request'

// 手册管理列表
export function apiDocumentLists(params: any) {
    return request.get({ url: '/help.document/lists', params })
}

// 添加手册管理
export function apiDocumentAdd(params: any) {
    return request.post({ url: '/help.document/add', params })
}

// 编辑手册管理
export function apiDocumentEdit(params: any) {
    return request.post({ url: '/help.document/edit', params })
}

// 删除手册管理
export function apiDocumentDelete(params: any) {
    return request.post({ url: '/help.document/delete', params })
}

// 手册管理详情
export function apiDocumentDetail(params: any) {
    return request.get({ url: '/help.document/detail', params })
}