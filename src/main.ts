import { createApp } from 'vue'
import App from './App.vue'
import install from './install'
import './permission'
import './styles/index.scss'
import 'virtual:svg-icons-register'
import VueUeditorWrap from 'vue-ueditor-wrap'

const app = createApp(App)
app.use(install).use(VueUeditorWrap)
app.mount('#app')
