## 项目说明

兔兔答题知识竞赛活动开源版本，使用THinkPHP6+Golang+Vue.2+Vue.3+TypeScript开发。兔兔答题是一款面向移动端答题的前后端应用程序，可用于考试活动，企业内部考核，内部培训等考试刷题。

## 项目特点

1、前端采用图鸟UI开发，支持编译到多端。支持 微信小程序、公众号H5、APP。

2、后端采用Go和PHP开发，支持高并发、高性能等业务场景。

3、管理端采用Element UI、Vue3和TypeScript，为前端等下流行技术栈。

4、数据库采用MySQL持久化数据存储，Redis作为缓存服务提高系统性能，为当前互联网热门技术栈。

## 仓库地址

数据库链接地址：[](https://pan.quark.cn/s/26f20687e8d8) 提取码：a9aw

代码仓库地址：[https://gitlab.com/tutudati1/tutudati001](https://gitlab.com/tutudati1/tutudati001)

兔兔官网地址：[https://www.tutudati.com/](https://www.tutudati.com/)

兔兔问答社区：[https://support.qq.com/products/620005](https://support.qq.com/products/620005)

插件市场：[https://ext.dcloud.net.cn/plugin?id=16028](https://ext.dcloud.net.cn/plugin?id=16028)

> 由于后端的包比较大，代码托管平台免费版对包大小的上传有限制，需要你添加官方客服获取后端代码地址。添加时备注“兔兔答题”。

## 页面预览

![](./image/tn-dati.jpg)
![](./image/tn-tutu.jpg)
![](./image/tn_author_qrcode.jpg)

## 如何使用

### 配置后端地址

```shell
开发环境下，直接将.env.development.example复制或者重命名为.env.development，将文件内的VITE_APP_BASE_URL变量值改成你实际后端的API地址。
正式环境下，直接将.env.production.example复制或者重命名为.env.production，将文件内的VITE_APP_BASE_URL变量值改成你实际后端的API地址。
```

### 安装依赖

```sh
npm install
```

### 开发环境运行

```sh
npm run dev
```

### 构建线上包

```sh
npm run build
```

### ESLint检测

```sh
npm run lint
```
